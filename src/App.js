import React ,{ Component } from 'react';
import './App.css';
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.state = { username: '' ,
          url:'https://f3sis6f6o8.execute-api.us-east-2.amazonaws.com/CI/toolbobx-managment?text=',
          text:''
          
      };
  }
  myChangeHandler = (event) => {
    this.setState({username: event.target.value});


  }

  handleClick() {
    console.log(this.state.username);
    /* axios.get(this.state.url+this.state.username)
    .then(res => {
      
      this.setState({ text:res.data.text });
      console.log(res.data.text);
    }) */

    var request = require('request-promise');
    
    var todayOptions = { 
      uri: this.state.url+this.state.username, 
      simple: false, 
      resolveWithFullResponse: true,
      json: true
    };
    request(todayOptions)
    .then(response => { this.setState({ text: (response.body.text) }) })
    .catch(function (err) {
      console.error(err);
    });
    
     
 
    
  }
  
  render() {
    return (
      <div>
        <form>
        <h1>  REACT - PROMISE  </h1>
        <p>Enter Text: {this.state.username}</p>
        <input
          type='text'
          onChange={this.myChangeHandler}
        />
        </form>
        <button className="btn btn-primary" onClick={() => this.handleClick()}>Send Text</button>
        <div>
          <p>Result Text:  {this.state.text}</p>
            
        </div>
       </div>
    );
  }
}

export default App;
